#!/usr/bin/env python
#################################################################################
#   bmpToField                                                                  #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import sys


def main(argv):
    """
    This is the plotting script of bmpToField.
    It plots the original image data and the resulting field.
    """
    #
    # Import
    #
    import getopt
    import os

    import bmptofield.context
    from bmptofield.configuration import FieldConfig

    #
    # Get image data
    #
    fc = FieldConfig()
    fc.getImage()

    #
    # Plot image channels
    #
    fc.imagetab.plotImageChannels(backend="ps", path=fc.data["CasePath"])


if __name__ == "__main__":
    main(sys.argv[1:])
