#!/usr/bin/env python
#################################################################################
#   bmpToField                                                                  #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import sys


def main(argv):
    """
    This is the configuration script of bmpToField.
    It sets up the input variables related to the case.
    """
    #
    # Import
    #
    import getopt
    import os

    import bmptofield.context
    from bmptofield.configuration import FieldConfig
    from lectio.input import userInputPrompt

    fc = FieldConfig()

    #
    # Input Case path
    #
    key = "CasePath"
    message = "Path to processed case:"
    if key in fc.data.keys():
        default = fc.data[key]
    else:
        default = "."
    fc.data[key] = os.path.abspath(userInputPrompt(message, default=default))

    #
    # Path to image
    #
    key = "imagePath"
    message = "Path to the image file:"
    if key in fc.data.keys():
        default = fc.data[key]
    else:
        default = "./image.bmp"
    fc.data[key] = os.path.abspath(userInputPrompt(message, default=default))

    #
    # Path to coordinate file
    #
    key = "coordinatePath"
    message = "Path to the coordinate file:"
    if key in fc.data.keys():
        default = fc.data[key]
    else:
        default = "./casename-COORD.mpio.txt"
    fc.data[key] = os.path.abspath(userInputPrompt(message, default=default))

    #
    # Mapping direction
    #
    key = "horizontalCoord"
    message = "The coordinate to map on horizontal direction of image:"
    if key in fc.data.keys():
        default = fc.data[key]
    else:
        default = "x"
    fc.data[key] = userInputPrompt(message, default=default)

    key = "verticalCoord"
    message = "The coordinate to map on vertical direction of image:"
    if key in fc.data.keys():
        default = fc.data[key]
    else:
        default = "y"
    fc.data[key] = userInputPrompt(message, default=default)

    #
    # Bounding box
    #
    key = "boundsHorizontalCoord"
    message = "Bounds of {} coordinate:".format(fc.data["horizontalCoord"])
    if key in fc.data.keys():
        default = fc.data[key]
    else:
        default = [-0.01, 0.01]
    fc.data[key] = userInputPrompt(message, typ="floatList", default=default)

    key = "boundsVerticalCoord"
    message = "Bounds of {} coordinate:".format(fc.data["verticalCoord"])
    if key in fc.data.keys():
        default = fc.data[key]
    else:
        default = [-0.01, 0.01]
    fc.data[key] = userInputPrompt(message, typ="floatList", default=default)

    #
    # List of fields to generate
    #
    key = "fieldList"
    message = "Fields to generate:"
    if key in fc.data.keys():
        default = fc.data[key]
    else:
        default = ["CON01.alya"]
    fc.data[key] = userInputPrompt(message, typ="strList", default=default)

    #
    # Channel selection
    #
    if not "fieldChannel" in fc.data.keys():
        fc.data["fieldChannel"] = {}
    if not "fieldBounds" in fc.data.keys():
        fc.data["fieldBounds"] = {}

    for field in fc.data["fieldList"]:
        #
        # Which channel to use
        #
        message = "Channel of {} field:".format(field)
        options = ["r", "g", "b", "w", "r_r", "g_r", "b_r", "w_r"]
        optDescr = [
            "Red part",
            "Green part",
            "Blue part",
            "White part (r+g+b)/3",
            "Reverse red",
            "Reverse green",
            "Reverse blue",
            "Reverse white",
        ]
        if field in fc.data["fieldChannel"].keys():
            default = fc.data["fieldChannel"][field]
        else:
            default = "w"
        fc.data["fieldChannel"][field] = userInputPrompt(
            message, default=default, options=options, optDescr=optDescr
        )

        #
        # Bounds of fiels
        #
        message = "Limits of {} field:".format(field)
        if field in fc.data["fieldBounds"].keys():
            default = fc.data["fieldBounds"][field]
        else:
            default = [0, 1]
        fc.data["fieldBounds"][field] = userInputPrompt(
            message, typ="floatList", default=default
        )

    #
    # Write global configuration
    #
    fc.writeConfig()


if __name__ == "__main__":
    main(sys.argv[1:])
