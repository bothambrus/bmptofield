init:
	pip3 install -r alyatab/requirements.txt
	pip3 install -r lectio/requirements.txt
	pip3 install -r requirements.txt

checkstyle:
	black --check bmptofield  
	black --check tests
	black --check configure
	black --check plot
	black --check map_to_fields
	isort --profile black --diff bmptofield
	isort --profile black --diff tests

style:
	black bmptofield 
	black tests
	black configure
	black plot
	black map_to_fields


test:
	rm -rf tests/tmp_testing/
	pytest --junitxml=junit_report.xml --cov=bmptofield --cov-report=html tests
	coverage xml
	mkdir -p dist 
	rm -rf dist/htmlcov
	mv coverage.xml dist/
	mv htmlcov dist/
	echo open dist/htmlcov/index.html to review code coverage
	python alyatab/.cicd_scripts/test_coverage.py 90 dist/coverage.xml



.PHONY: init test

