#################################################################################
#   bmpToField                                                                  #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import json
import shutil
import numpy as np

from .context import bmptofield, getTestPath, getTestTmpPath
from bmptofield.configuration import FieldConfig


test_path = getTestPath()
tmp_test_path = getTestTmpPath()


def test_create_FieldConfig():
    """
    Test creation of configuraton object.
    """
    fc = FieldConfig()


def test_read_image_FieldConfig():
    """
    Test reading image into configuraton object.
    """
    fc = FieldConfig()
    fc.data["imagePath"] = os.path.join(test_path, "data", "example_bitmap_file.bmp")
    fc.getImage()

    assert fc.imagetab.shape == (150, 200)


def test_get_all_data_FieldConfig():
    """
    Test getting all data of configuraton object.
    """
    fc = FieldConfig()
    fc.data["imagePath"] = os.path.join(test_path, "data", "example_bitmap_file.bmp")
    fc.data["coordinatePath"] = os.path.join(test_path, "data", "example.coord")
    fc.data["horizontalCoord"] = "x"
    fc.data["verticalCoord"] = "y"
    fc.data["boundsHorizontalCoord"] = [-0.01, 0.01]
    fc.data["boundsVerticalCoord"] = [-0.01, 0.01]
    fc.getData()

    assert fc.imagetab.shape == (150, 200)

    assert fc.coords.shape == (40401, 3)
    assert np.abs(np.max(fc.coords[:, 0]) - 40401) < 1e-6
    assert np.abs(np.max(fc.coords[:, 1]) - 0.01) < 1e-6
    assert np.abs(np.max(fc.coords[:, 2]) - 0.01) < 1e-6
    assert np.abs(np.min(fc.coords[:, 1]) - -0.01) < 1e-6
    assert np.abs(np.min(fc.coords[:, 2]) - -0.01) < 1e-6
    assert np.all(np.abs(fc.coords[1000, :] - np.array([1001, 0.0098, 0.0098])) < 1e-6)
    assert np.all(
        np.abs(fc.coords[20000, :] - np.array([20001, 0.0003, 0.0003])) < 1e-6
    )
    assert np.all(np.abs(fc.coords[30500, :] - np.array([30501, -0.005, 0.005])) < 1e-6)

    assert fc.lookupArray.shape == (40401, 2)
    assert np.abs(np.max(fc.lookupArray[:, 0]) - 1.0) < 1e-6
    assert np.abs(np.min(fc.lookupArray[:, 0]) - 0.0) < 1e-6
    assert np.abs(np.max(fc.lookupArray[:, 1]) - 1.0) < 1e-6
    assert np.abs(np.min(fc.lookupArray[:, 1]) - 0.0) < 1e-6
    assert np.all(np.abs(fc.lookupArray[1000, :] - np.array([0.99, 0.99])) < 1e-6)
    assert np.all(np.abs(fc.lookupArray[20000, :] - np.array([0.515, 0.515])) < 1e-6)
    assert np.all(np.abs(fc.lookupArray[30500, :] - np.array([0.25, 0.75])) < 1e-6)


def test_read_json_FieldConfig():
    """
    Test creation of configuraton object with given json path.
    """
    test_case = os.path.join(tmp_test_path, "config_read_json")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path = os.path.join(test_case, "bmpToFieldInputFile.json")

    #
    # Write json file manually
    #
    data = {}
    data["imagePath"] = os.path.join(test_path, "data", "example_bitmap_file.bmp")
    with open(json_path, "w") as f:
        json.dump(data, f)

    #
    # Initialize with given json path
    #
    fc = FieldConfig(json_path=json_path)

    assert fc.imagetab.shape == []
    assert "example_bitmap_file.bmp" in fc.data["imagePath"]


def test_write_json_FieldConfig():
    """
    Test writing configuration data into json path.
    """
    test_case = os.path.join(tmp_test_path, "config_write_json")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)
    json_path = os.path.join(test_case, "bmpToFieldInputFile.json")

    #
    # Fill with data
    #
    fc = FieldConfig(json_path=json_path)
    fc.data["imagePath"] = os.path.join(test_path, "data", "example_bitmap_file.bmp")

    #
    # Test writing
    #
    fc.writeConfig()

    #
    # Check
    #
    with open(json_path, "r") as f:
        data = json.load(f)
    assert "example_bitmap_file.bmp" in data["imagePath"]


def test_map_to_field_FieldConfig():
    """
    Test mapping image data onto mesh file.
    """
    #
    # Case
    #
    test_case = os.path.join(tmp_test_path, "map_image_to_field")
    if not os.path.isdir(test_case):
        os.makedirs(test_case)

    #
    # Initialize
    #
    fc = FieldConfig()
    fc.data["imagePath"] = os.path.join(test_path, "data", "example_bitmap_file.bmp")
    fc.data["coordinatePath"] = os.path.join(test_path, "data", "example.coord")
    fc.data["horizontalCoord"] = "x"
    fc.data["verticalCoord"] = "y"
    fc.data["boundsHorizontalCoord"] = [-0.01, 0.01]
    fc.data["boundsVerticalCoord"] = [-0.01, 0.01]
    fc.getData()

    #
    # Write field
    #
    fc.mapAndSaveField(test_case, "field1", "w", [0, 1])
    fc.mapAndSaveField(test_case, "field2", "r_r", [0.8, 0.9])

    #
    # Test results
    #
    data1 = np.loadtxt(os.path.join(test_case, "field1"))
    data2 = np.loadtxt(os.path.join(test_case, "field2"))

    assert np.abs(np.max(data1[:, 0]) - 40401) < 1e-6
    assert np.abs(np.min(data1[:, 0]) - 1) < 1e-6
    assert np.abs(np.max(data1[:, 1]) - 1.0) < 1e-6
    assert np.abs(np.min(data1[:, 1]) - 0.0) < 1e-6

    assert np.abs(data1[1000, 1] - 1.0) < 1e-6
    assert np.abs(data1[20000, 1] - 1.0) < 1e-6
    assert np.abs(data1[30500, 1] - 1.0) < 1e-6

    assert np.abs(np.max(data2[:, 0]) - 40401) < 1e-6
    assert np.abs(np.min(data2[:, 0]) - 1) < 1e-6
    assert np.abs(np.max(data2[:, 1]) - 0.9) < 1e-6
    assert np.abs(np.min(data2[:, 1]) - 0.8) < 1e-6

    assert np.abs(data2[1000, 1] - 0.8) < 1e-6
    assert np.abs(data2[20000, 1] - 0.8) < 1e-6
    assert np.abs(data2[30500, 1] - 0.8) < 1e-6
