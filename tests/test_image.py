#################################################################################
#   bmpToField                                                                  #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
import json
import shutil
import numpy as np

from .context import bmptofield, getTestPath, getTestTmpPath
from bmptofield.image import ImageTable

test_path = getTestPath()
tmp_test_path = getTestTmpPath()


def test_create_ImageTable():
    """
    Test creation of image table.
    """
    tab = ImageTable(typ="single")
    tab = ImageTable(typ="double")


def test_reading_image():
    """
    Test creation of image table with an image file.
    """
    tab = ImageTable(
        fn=os.path.join(test_path, "data", "example_bitmap_file.bmp"), typ="single"
    )

    case_path = os.path.join(tmp_test_path, "image_reading")
    shutil.rmtree(case_path, ignore_errors=True)
    if not os.path.exists(case_path):
        os.makedirs(case_path)

    #
    # Write table for debugging
    #
    tab_path = os.path.join(case_path, "image.dat")
    tab.writeText(tab_path)

    #
    # Check table structure
    #
    assert tab.coordKeys == ["horiz", "vert"]
    assert len(tab.coords["horiz"]) == 150
    assert len(tab.coords["vert"]) == 200
    assert tab.coords["horiz"][0] == 0.0
    assert tab.coords["vert"][0] == 0.0
    assert tab.coords["horiz"][149] == 1.0
    assert tab.coords["vert"][199] == 1.0
    assert tab.shape == (150, 200)

    #
    # Interpolate in data
    #
    valarr = np.zeros([5, 2])
    valarr[0, :] = [0.24, 0.57]  # R
    valarr[1, :] = [0.43, 0.22]  # G
    valarr[2, :] = [0.75, 0.70]  # B
    valarr[3, :] = [0.80, 0.38]  # K
    valarr[4, :] = [0.50, 0.50]  # W

    goal_r = np.zeros([5])
    goal_r[0] = 1.0
    goal_r[1] = 0.0
    goal_r[2] = 0.0
    goal_r[3] = 0.0
    goal_r[4] = 1.0

    goal_g = np.zeros([5])
    goal_g[0] = 0.0
    goal_g[1] = 1.0
    goal_g[2] = 0.14901961
    goal_g[3] = 0.0
    goal_g[4] = 1.0

    goal_b = np.zeros([5])
    goal_b[0] = 0.0
    goal_b[1] = 0.12941177
    goal_b[2] = 1.0
    goal_b[3] = 0.0
    goal_b[4] = 1.0

    data = tab.interpolateArray(valarr, key_list=["r", "g", "b"])

    assert np.all(np.abs(data["r"] - goal_r) < 1e-6)
    assert np.all(np.abs(data["g"] - goal_g) < 1e-6)
    assert np.all(np.abs(data["b"] - goal_b) < 1e-6)


def test_plottinging_image():
    """
    Test plotting image for inspection.
    """
    tab = ImageTable(
        fn=os.path.join(test_path, "data", "example_bitmap_file.bmp"), typ="single"
    )

    case_path = os.path.join(tmp_test_path, "image_plotting")
    shutil.rmtree(case_path, ignore_errors=True)
    if not os.path.exists(case_path):
        os.makedirs(case_path)

    #
    # Test plotting
    #
    tab.plotImageChannels(backend="ps", path=case_path)
    assert os.path.isfile(os.path.join(case_path, "ImageChannels.eps"))
