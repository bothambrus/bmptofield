# bmpToField

Turn a bitmap image into a field for [Alya](https://gitlab.com/bsc-alya/alya).

For details on installation and usage, see the [wiki](https://gitlab.com/bothambrus/bmptofield/-/wikis/home).
