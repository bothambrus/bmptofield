#################################################################################
#   bmpToField                                                                  #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import imageio
import os

import numpy as np

import bmptofield.context
from alyatab.table import AlyaTable


class ImageTable(AlyaTable):
    """
    Class to hold tabulated image data
    """

    def __init__(self, fn=None, typ="single"):

        #
        # Default values
        #
        dic = {}
        keys = []
        coordKeys = []
        coords = {}

        #
        # Initialize parent class
        #
        AlyaTable.__init__(
            self, dic=dic, keys=keys, coordKeys=coordKeys, coords=coords, typ=typ
        )

        #
        # If file exists, try reading it as bitmap
        #
        self.image = None
        self.setImage(fn)

    def setImage(self, fn):
        """
        Set wef file as the image of the object.
        """
        if (not fn is None) and os.path.isfile(fn):
            self.image = imageio.imread(fn)

        self.processImage()

    def processImage(self):
        """
        Process current image.
        """
        if not self.image is None:
            #
            # Process this image
            #
            dic = {}
            keys = []
            coordKeys = []
            coords = {}

            #
            # Image shape
            #
            coordKeys = ["horiz", "vert"]
            coords["horiz"] = np.linspace(0, 1, self.image.shape[1])
            coords["vert"] = np.linspace(0, 1, self.image.shape[0])

            #
            # Image data
            #
            keys = ["r", "g", "b"]
            for ik, k in enumerate(keys):
                #
                # Normalize
                #
                dic[k] = np.flip(np.transpose(self.image[:, :, ik] / 255.0), axis=1)

            #
            # Re-initialize parent class
            #
            AlyaTable.__init__(
                self,
                dic=dic,
                keys=keys,
                coordKeys=coordKeys,
                coords=coords,
                typ=self.dtype,
            )

    def plotImageChannels(self, backend="default", path="."):
        """
        Plot current image.
        """
        #
        # Set backend
        #
        import matplotlib

        if backend != "default":
            matplotlib.use(backend)
        import matplotlib.pyplot as plt

        #
        # Get figure
        #
        fig, axs = plt.subplots(1, 4)
        fig.set_size_inches(12, 3.5)
        fig.subplots_adjust(
            top=0.95, right=0.98, bottom=0.05, left=0.02, wspace=0.1, hspace=0.0
        )

        #
        # Get coordinates:
        #
        x = np.zeros_like(self.data["r"])
        for i, xi in enumerate(self.coords["horiz"]):
            x[i, :] = xi
        y = np.zeros_like(self.data["r"])
        for i, yi in enumerate(self.coords["vert"]):
            y[:, i] = yi

        for iax, ax in enumerate(axs):
            #
            # Select property
            #
            cmap = "jet"
            if iax == 0:
                z = self.data["r"]
                ax.set_title("R")
                cmap = "Reds"
            elif iax == 1:
                z = self.data["g"]
                ax.set_title("G")
                cmap = "Greens"
            elif iax == 2:
                z = self.data["b"]
                ax.set_title("B")
                cmap = "Blues"
            else:
                z = (self.data["r"] + self.data["g"] + self.data["b"]) / 3.0
                ax.set_title("W")
                cmap = "Greys_r"

            #
            # Plot
            #
            ax.contourf(x, y, z, cmap=cmap)

            #
            # Ticks
            #
            ax.set_xticks([])
            ax.set_yticks([])

            #
            # Aspect rato
            #
            ax.set_aspect("equal", "box")

        #
        # Save figure
        #
        fig.savefig(os.path.join(path, "ImageChannels.eps"), format="eps", dpi=1200)
