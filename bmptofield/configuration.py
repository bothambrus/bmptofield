#################################################################################
#   bmpToField                                                                  #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import json
import os

import numpy as np

from bmptofield.image import ImageTable


class FieldConfig:
    """
    Class to hold configuration data of image mapping to field.
    """

    def __init__(self, json_path=os.path.abspath("bmpToFieldInputFile.json")):
        #
        # Initialize data
        #
        self.data = {}
        self.json_path = json_path

        #
        # Read json
        #
        if os.path.isfile(self.json_path):
            #
            # Read if exists
            #
            with open(self.json_path, "r") as f:
                self.data = json.load(f)

        #
        # Initialize data
        #
        self.imagetab = ImageTable()

    def writeConfig(self):
        """
        Write to path
        """
        with open(self.json_path, "w") as f:
            json.dump(self.data, f, sort_keys=True, indent=4)

    def getData(self):
        """
        Get all available data
        """
        self.getImage()
        self.getCoordinates()

    def getImage(self):
        """
        Load image into table.
        """
        self.imagetab = ImageTable()
        if "imagePath" in self.data.keys():
            self.imagetab.setImage(self.data["imagePath"])

    def getCoordinates(self):
        """
        Load coordinate list.
        """
        #
        # Read coordinates
        #
        self.coords = None
        if "coordinatePath" in self.data.keys():
            self.coords = np.loadtxt(self.data["coordinatePath"], comments="$")

        #
        # Prepare lookup array
        #
        self.lookupArray = None
        if (
            "horizontalCoord" in self.data.keys()
            and "verticalCoord" in self.data.keys()
            and "boundsHorizontalCoord" in self.data.keys()
            and "boundsVerticalCoord" in self.data.keys()
        ):
            npoin = self.coords.shape[0]
            self.lookupArray = np.empty([npoin, 2])

            #
            # Coordinate corresponding to horizontal part
            #
            col = ["x", "y", "z"].index(self.data["horizontalCoord"]) + 1
            self.lookupArray[:, 0] = (
                self.coords[:, col] - self.data["boundsHorizontalCoord"][0]
            ) / (
                self.data["boundsHorizontalCoord"][1]
                - self.data["boundsHorizontalCoord"][0]
            )
            self.lookupArray[:, 0] = np.where(
                self.lookupArray[:, 0] > 1.0, 1.0, self.lookupArray[:, 0]
            )
            self.lookupArray[:, 0] = np.where(
                self.lookupArray[:, 0] < 0.0, 0.0, self.lookupArray[:, 0]
            )

            #
            # Coordinate corresponding to vertical part
            #
            col = ["x", "y", "z"].index(self.data["verticalCoord"]) + 1
            self.lookupArray[:, 1] = (
                self.coords[:, col] - self.data["boundsVerticalCoord"][0]
            ) / (
                self.data["boundsVerticalCoord"][1]
                - self.data["boundsVerticalCoord"][0]
            )
            self.lookupArray[:, 1] = np.where(
                self.lookupArray[:, 1] > 1.0, 1.0, self.lookupArray[:, 1]
            )
            self.lookupArray[:, 1] = np.where(
                self.lookupArray[:, 1] < 0.0, 0.0, self.lookupArray[:, 1]
            )

    def mapAndSaveField(self, path, field, channel, bounds):
        """
        Map image data defined by channel onto coordinates and scale it by the given bounds.
        """

        if not self.lookupArray is None:
            if channel[0] == "w":
                key_list = ["r", "g", "b"]
            else:
                key_list = [channel[0]]

            #
            # Lookup
            #
            data = self.imagetab.interpolateArray(self.lookupArray, key_list=key_list)

            #
            # Construct
            #
            if channel[0] == "w":
                outdata = (data["r"] + data["g"] + data["b"]) / 3.0
            else:
                outdata = data[channel[0]]

            #
            # Manipulate
            #
            if "_r" in channel:
                outdata = 1.0 - outdata

            #
            # Scale
            #
            outdata = bounds[0] + (bounds[1] - bounds[0]) * outdata

            #
            # Write
            #
            fn = os.path.join(path, field)
            with open(fn, "w") as f:
                for i, d in enumerate(outdata):
                    line = "{} {}\n".format(i + 1, d)
                    f.write(line)
