import sys
import math
import numpy as np


meshName=sys.argv[1]


coordfile       = '{}.coord'.format(meshName)
velofile        = 'VELOC.alya'

fCoord          =open(coordfile,'r')
fVel            =open(velofile,'w')


print('---| Start writing initial condition')
for line in fCoord:
    data=line.split()

    pid = int(data[0])
    dims = len(data)-1
    x   = float(data[1])
    y   = float(data[2])
    z   = float(data[3])

    vx = 10.0
    vy = 0.0
    vz = 0.0

    fVel.write('{} {} {} {}\n'.format(pid,vx,vy,vz))
        


fCoord.close()
fVel.close()
print('---| End writing initial condition')


