name='mesh3d'
gmsh -format msh2 -3 $name".geo"
gmsh2alya.pl $name -bcs=boundaries
python getCoordinates.py $name
python initialCondition.py $name 
python getRealBoundaries.py $name 2

sed -i -e '1d; $d' $name".fix.bou" 

